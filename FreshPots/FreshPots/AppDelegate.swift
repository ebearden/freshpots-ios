//
//  AppDelegate.swift
//  FreshPots
//
//  Created by FreshPots on 9/26/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // This needs to be moved to its own style controller.
        UITabBar.appearance().tintColor = UIColor(red:0.2, green:0.22, blue:0.33, alpha:1)
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        let userScope = "https://www.googleapis.com/auth/userinfo.profile"
        let currentScopes: NSArray = GIDSignIn.sharedInstance().scopes
        GIDSignIn.sharedInstance().scopes = currentScopes.arrayByAddingObject(userScope)
        
        
        let isLoggedIn: Bool = NSUserDefaults.standardUserDefaults().boolForKey("IS_LOGGED_IN");
        if isLoggedIn {
            GIDSignIn.sharedInstance().signInSilently()
        }
        else {
            let storyboardId: String = isLoggedIn == true ? "TabBarController" : "LoginViewController";
            self.window?.rootViewController = self.window?.rootViewController?.storyboard?.instantiateViewControllerWithIdentifier(storyboardId);
        }
        
        return true
    }
    
    func application(application: UIApplication,
        openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
            return GIDSignIn.sharedInstance().handleURL(url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    }

    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
        withError error: NSError!) {
            if error == nil {
                // Perform any operations on signed in user here.
                let userId = user.userID                  // For client-side use only!
                let idToken = user.authentication.idToken // Safe to send to the server
                let accessToken = user.authentication.accessToken
                let name = user.profile.name
                let email = user.profile.email
                
                print("userId: \(userId)")
                print("idToken: \(idToken)")
                print("accessToken: \(accessToken)")
                print("name: \(name)")
                print("email: \(email)")
                
                //TODO: Sign up the user against our server
                
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "IS_LOGGED_IN")
                
                self.window?.rootViewController = self.window?.rootViewController?.storyboard?.instantiateViewControllerWithIdentifier("TabBarController");
                
                let url: NSURL = NSURL(string: "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=\(accessToken)")!
                let request: NSURLRequest = NSURLRequest(URL: url)
                let session: NSURLSession = NSURLSession.sharedSession()
                session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                    if let errorResponse = error {
                        print("error received: \(errorResponse)")
                        return;
                    }
                    
                    if let jsonData = data {
                        let json = JSON(data: jsonData)
                        print("json user response: \(json)")
                        
                        //TODO: Notify the server of profile details
                    }
                }).resume()
            }
            else {
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IS_LOGGED_IN")
                
                self.window?.rootViewController = self.window?.rootViewController?.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController");
                
                print("\(error.localizedDescription)")
            }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
        withError error: NSError!) {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IS_LOGGED_IN")
            
            self.window?.rootViewController = self.window?.rootViewController?.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController");
    }
}

