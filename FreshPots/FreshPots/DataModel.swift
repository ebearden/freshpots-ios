//
//  DataModel.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/10/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
    Protocol that Models using the `DataModelService` must conform to.
*/
protocol DataModel {
    
    /// The identifier of the `DataModel`
    var identifier :String { get set }
    
    /// Returns a dictionary representation of the `DataModel`
    func jsonDictionary() -> Dictionary<String, AnyObject>
    
    /// Returns an object that conforms to the `DataModel` protocol.
    /// - Parameter json: A SwiftyJSON object.
    static func make(json: JSON) -> DataModel
}

/// TypeAlias for a closure `(JSON) -> DataModel`
typealias ModelFactory = (JSON) -> DataModel