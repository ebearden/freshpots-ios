//
//  DataModelFactory.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/10/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

enum DataModelType {
    case User, Team, Pot, Room
}


/**
    Responsible for factories of `DataModel` type.
*/
class DataModelFactory {
    /// Returns a `ModelFactory` for the specified `DataModelType`
    /// - Parameter type: A `DataModelType`
    class func factoryFor(type: DataModelType) -> ModelFactory {
        switch type {
        case .User:
            return User.make
        case .Team:
            return Team.make
        case .Pot:
            return Pot.make
        case .Room:
            return Room.make
        }
    }
}
