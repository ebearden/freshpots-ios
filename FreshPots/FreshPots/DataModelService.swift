//
//  DataModelService.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/11/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
    Generic class for connecting to the FreshPots REST HTTPController and 
    returning specified objects that conform to the `DataModel` protocol.
*/

class DataModelService<T:DataModel>: NSObject {
    
    /// Get all objects of the `DataModel` type.
    ///
    class func getModels(completion: ([T]) -> Void) {
        let httpController = HTTPController(urlSession: NSURLSession.sharedSession())
        var items = [T]()
        
        httpController.GET(createUrl(T.self, endpoint: ""), params: nil) { data in
            let json = self.objectFromJSON(data)
            let factory = DataModelFactory.factoryFor(dataModelTypeForGeneric(T.self))
            let itemArray = JSON(json!)
            
            for (_, subJson):(String, JSON) in itemArray {
                let item = factory(subJson)
                items.append(item as! T)
            }
            
            completion(items)
        }
    }
    
    /// Get a specific object of the `DataModel` type.
    /// - Parameter id: The objects identifier.
    ///
    class func getModelWithId(id: String, completion: (T) -> Void) {
        let httpController = HTTPController(urlSession: NSURLSession.sharedSession())
        
        httpController.GET(createUrl(T.self, endpoint:"/\(id)"), params: nil) { data in
            let json = self.objectFromJSON(data)
            let factory = DataModelFactory.factoryFor(dataModelTypeForGeneric(T.self))
            let item = factory(JSON(json!)[keyForGeneric(T.self)]) as! T
            completion(item)
        }
    }
    
    /// Add or update an object of the `DataModel` type.
    /// If the passed in model has an identifier update the model, else add a new model.
    /// - Parameter model: A model that conforms to the `DataModel` protocol.
    ///
    class func saveModel(model: T, completion: (Bool, model: T?) -> Void) {
        if model.identifier.isEmpty {
            createModel(model, completion: completion)
        }
        else {
            updateModel(model, completion: completion)
        }
    }
    
    private class func createModel(model: T, completion:(Bool, model :T?) -> Void) {
        let httpController = HTTPController(urlSession: NSURLSession.sharedSession())
        
        httpController.POST(createUrl(T.self, endpoint: ""), params: model.jsonDictionary()) { data in
            let json = self.objectFromJSON(data)
            let jsonDict = JSON(json!)
            
            if jsonDict["status"].stringValue == "success" {
                let factory = DataModelFactory.factoryFor(dataModelTypeForGeneric(T.self))
                let item = factory(JSON(json!)[keyForGeneric(T.self)]) as! T
                completion(true, model: item)
            }
            else {
                completion(false, model:nil)
            }
        }
    }
    
    private class func updateModel(model: T, completion:(Bool, model :T?) -> Void) {
        let httpController = HTTPController(urlSession: NSURLSession.sharedSession())
        
        httpController.POST(createUrl(T.self, endpoint: "/\(model.identifier)"),
            params: model.jsonDictionary()) { data in
            let json = self.objectFromJSON(data)
            let jsonDict = JSON(json!)
            
            if jsonDict["status"].stringValue == "success" {
                let factory = DataModelFactory.factoryFor(dataModelTypeForGeneric(T.self))
                let item = factory(JSON(json!)[keyForGeneric(T.self)]) as! T
                completion(true, model: item)
            }
            else {
                completion(false, model: nil)
            }
        }
    }
    
    /// Delete a specific object of the `DataModel` type.
    /// - Parameter id: The objects identifier.
    ///
    class func deleteModel(model: T, completion:(Bool) -> Void) {
        let httpController = HTTPController(urlSession: NSURLSession.sharedSession())
        
        httpController.DELETE(createUrl(T.self, endpoint: "/\(model.identifier)"), params: nil) { data in
            let json = self.objectFromJSON(data)
            let jsonDict = JSON(json!)
            
            if jsonDict["status"].stringValue == "success" {
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
}

// MARK: - Private Helpers -
extension DataModelService {
    private class func dataModelTypeForGeneric(type: Any) -> DataModelType {
        switch type {
        case is User.Type:
            return .User
        case is Team.Type:
            return .Team
        case is Pot.Type:
            return .Pot
        case is Room.Type:
            return .Room
        default: return .User
        }
    }
    
    private class func keyForGeneric(genericType: Any) -> String {
        switch genericType {
        case is User.Type:
            return "user"
        case is Team.Type:
            return "team"
        case is Pot.Type:
            return "pot"
        case is Room.Type:
            return "room"
        default:
            return ""
        }
    }
    
    private class func createUrl(genericType: Any, endpoint: String) -> NSURL {
        let baseUrl = "http://rest.nerdsahoy.com"
        var typeUrlPart :String
        
        switch genericType {
        case is User.Type:
            typeUrlPart = "/user"
        case is Team.Type:
            typeUrlPart = "/team"
        case is Pot.Type:
            typeUrlPart = "/pot"
        case is Room.Type:
            typeUrlPart = "/room"
        default:
            typeUrlPart = ""
        }

        return NSURL(string: "\(baseUrl)\(typeUrlPart)\(endpoint)")!
    }
    
    private class func objectFromJSON(data: NSData?) -> AnyObject? {
        guard data != nil else {
            print("Data is nil")
            return nil
        }
        
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            return json
        }
        catch let parseError {
            print(parseError)
        }
        
        return nil
    }
}
