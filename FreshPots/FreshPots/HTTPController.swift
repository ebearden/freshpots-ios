//
//  HTTPController.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/5/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

/**
    Basic HTTPController with GET, POST, PUT and DELETE methods
*/
class HTTPController: NSObject {
    private var urlSession: NSURLSession
    private var completion: ((data: NSData) -> Void)!
    
    init(urlSession: NSURLSession) {
        self.urlSession = urlSession
    }
    
    func GET(url: NSURL, params: Dictionary<String, AnyObject>?, completion: ((data: NSData) -> Void)) {
        self.completion = completion
        
        if let bodyParams = params {
            let body = encodeDictionary(bodyParams)
            connectionWithHTTPMethod("GET", url: url, body: body)
        }
        else {
            connectionWithHTTPMethod("GET", url: url, body: nil)
        }
    }
    
    func POST(url: NSURL, params: Dictionary<String, AnyObject>?, completion: ((data: NSData) -> Void)) {
        self.completion = completion
        
        if let bodyParams = params {
            let body = encodeDictionary(bodyParams)
            connectionWithHTTPMethod("POST", url: url, body: body)
        }
        else {
            connectionWithHTTPMethod("POST", url: url, body: nil)
        }
    }
    
    func PUT(url: NSURL, params: Dictionary<String, AnyObject>?, completion: ((data: NSData) -> Void)) {
        self.completion = completion
        
        if let bodyParams = params {
            let body = encodeDictionary(bodyParams)
            connectionWithHTTPMethod("PUT", url: url, body: body)
        }
        else {
            connectionWithHTTPMethod("PUT", url: url, body: nil)
        }
    }
    
    func DELETE(url: NSURL, params: Dictionary<String, AnyObject>?, completion: ((data: NSData) -> Void)) {
        self.completion = completion
        
        if let bodyParams = params {
            let body = encodeDictionary(bodyParams)
            connectionWithHTTPMethod("DELETE", url: url, body: body)
        }
        else {
            connectionWithHTTPMethod("DELETE", url: url, body: nil)
        }
    }
}

// MARK: - Private Methods -
extension HTTPController {
    private func encodeDictionary(dictionary: Dictionary<String, AnyObject>) -> NSData {
        do {
            let data = try! NSJSONSerialization.dataWithJSONObject(dictionary, options: .PrettyPrinted)
            return data
        }
    }
    
    private func connectionWithHTTPMethod(method: String, url: NSURL, body: NSData?) {
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = method
        request.HTTPBody = body
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let dataTask = self.urlSession.dataTaskWithRequest(request, completionHandler: completionHandler)
        dataTask.resume()
    }
    
    private func completionHandler(data: NSData?, response: NSURLResponse?, error: NSError?) {
        if let returnData = data {
            if completion != nil {
                completion(data: returnData)
            }
        }
    }
}
