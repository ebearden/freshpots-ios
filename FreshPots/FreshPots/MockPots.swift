//
//  MockPots.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/30/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import Foundation

struct MockPots {
    static func getPots() -> [Pot] {
        var pots = [Pot]()
        for i in 1...10 {
            let pot = Pot(
                identifier: "\(i)",
                name: "Pot \(i)",
                roomId: "\(i)",
                freshCount: 5,
                lastPot: "Never!")
            
            pots.append(pot)
        }
        
        return pots
    }
}
