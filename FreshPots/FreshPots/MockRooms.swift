//
//  MockRooms.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/30/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import Foundation

struct MockRooms {
    static func getRooms() -> [Room] {
        var rooms = [Room]()
        
        for i in 1...10 {
            let room = Room(
                identifier: "\(i)",
                name: "Room \(i)",
                teamId: "\(i)")
            
            rooms.append(room)
        }
        
        return rooms
    }
}