//
//  MockTeams.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/29/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import Foundation

struct MockTeams {
    static func getTeams() -> [Team] {
        var teams = [Team]()
        
        for i in 1...10 {
            let team = Team(
                identifier: "\(i)",
                name: "Team \(i)",
                profileImage: "",
                location: "Location \(i)")
            
            teams.append(team)
        }
        
        return teams
    }
}