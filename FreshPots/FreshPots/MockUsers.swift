//
//  MockUsers.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/30/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import Foundation

struct MockUsers {
    static func getUsers() -> [User] {
        var users = [User]()
        
        for i in 1...10 {
            let user = User(
                identifier: "",
                emailAddress: "user\(i)@test.com",
                firstName: "First\(i)",
                lastName: "Last\(i)",
                profileImage: "")
            
            users.append(user)
        }
        
        return users
    }
}
