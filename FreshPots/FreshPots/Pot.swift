//
//  Pot.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/26/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Pot : DataModel, CustomStringConvertible {
    /// Identifier generated from server
    var identifier :String
    
    /// The name of the pot
    var name :String
    
    /// The id of the room the pot belongs to
    var roomId :String
    
    /// The number of fresh pots made
    var freshCount :Int
    
    /// The datetime of the last fresh pot
    var lastPot :String
}

// MARK: - DataModel Protocol -
extension Pot {
    static func make(json: JSON) -> DataModel {
        return Pot(identifier: json["id"].stringValue,
            name: json["name"].stringValue,
            roomId: json["room_id"].stringValue,
            freshCount: json["fresh_count"].intValue,
            lastPot: json["last_pot"].stringValue
        )
    }
    
    func jsonDictionary() -> Dictionary<String, AnyObject> {
        return [
            "id": identifier,
            "name": name,
            "room_id": roomId,
            "fresh_count": freshCount,
            "last_pot": lastPot
        ]
    }
}

// MARK: - CustomStringConvertible -
extension Pot {
    var description :String {
        return "id: \(identifier)\name: \(name)\nroomId: \(roomId)\nfreshCount:\(freshCount)\nlastPot:\(lastPot)"
    }
}
