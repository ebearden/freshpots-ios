//
//  PotTableViewCell.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/13/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

class PotTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var teamLabel: UILabel!
    @IBOutlet var roomLabel: UILabel!
    
    private var pot :Pot?
    
    func setPot(pot: Pot) {
        nameLabel.text = pot.name
        roomLabel.text = pot.roomId
    }
}
