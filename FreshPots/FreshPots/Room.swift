//
//  Room.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/26/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Room : DataModel, CustomStringConvertible {
    /// Identifier generated from server
    var identifier :String
    
    /// The name of the room
    var name :String
    
    /// Then id of the team the room belongs to.
    var teamId :String
}

// MARK: - DataModel Protocol -
extension Room {
    static func make(json: JSON) -> DataModel {
        return Room(identifier: json["id"].stringValue,
            name: json["name"].stringValue,
            teamId: json["team_id"].stringValue
        )
    }
    
    func jsonDictionary() -> Dictionary<String, AnyObject> {
        return [
            "id": identifier,
            "name": name,
            "team_id": teamId
        ]
    }
}

// MARK: - CustomStringConvertible -
extension Room {
    var description :String {
        return "Id: \(identifier)\name: \(name)\nteamId: \(teamId)"
    }
}
