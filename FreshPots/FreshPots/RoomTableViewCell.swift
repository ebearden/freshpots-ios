//
//  RoomTableViewCell.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/19/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

class RoomTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!

    private var room :Room?
    
    func setRoom(room: Room) {
        nameLabel.text = room.name
    }
}
