//
//  SearchViewController.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/26/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController, UISearchBarDelegate,
                            UISearchResultsUpdating, SearchViewModelDelegate {
    private let viewModel = SearchViewModel()
    private var searchController :UISearchController!
}

// MARK: - TableView -
extension SearchViewController {
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return viewModel.numberOfSectionsInTableView()
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.active {
            return viewModel.titleForFilteredSection(section)
        }
        else {
            return viewModel.titleForSection(section)
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.active {
            return viewModel.numberOfRowsInFilteredSection(section)
        }
        else {
            return viewModel.numberOfRowsInSection(section)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var item :DataModel
        if self.searchController.active {
            item = viewModel.itemForFilteredRowAtIndexPath(indexPath)!
        }
        else {
            item = viewModel.itemForRowAtIndexPath(indexPath)!
        }
        
        return cellForItemAtIndexPath(item, indexPath: indexPath)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

// MARK: - SearchBar -
extension SearchViewController {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        viewModel.filterContentForSearchText(searchController.searchBar.text!,
            scope: searchController.searchBar.selectedScopeButtonIndex)
        tableView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResultsForSearchController(searchController)
    }
}

// MARK: - SearchViewModelDelegate -
extension SearchViewController {
    func viewModelUpdated() {
        tableView.reloadData()
    }
}

// MARK: - Lifecycle -
extension SearchViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        
        setupSearchController()
        self.definesPresentationContext = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

// MARK: - Private -
extension SearchViewController {
    private func setupSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.scopeButtonTitles = viewModel.scopeButtonTitles()
        
        tableView.tableHeaderView = searchController.searchBar
    }
    
    private func cellForItemAtIndexPath(item: DataModel, indexPath: NSIndexPath) -> UITableViewCell {
        switch item {
        case is Pot:
            let cell = tableView.dequeueReusableCellWithIdentifier("PotTableViewCell",
                forIndexPath: indexPath) as! PotTableViewCell
            
            if let pot = item as? Pot {
                cell.setPot(pot)
            }
            
            return cell
            
        case is User:
            let cell = tableView.dequeueReusableCellWithIdentifier("UserTableViewCell",
                forIndexPath: indexPath) as! UserTableViewCell
            
            if let user = item as? User {
                cell.setUser(user)
            }
            
            return cell
            
        case is Team:
            let cell = tableView.dequeueReusableCellWithIdentifier("TeamTableViewCell",
                forIndexPath: indexPath) as! TeamTableViewCell
            
            if let team = item as? Team {
                cell.setTeam(team)
            }
            
            return cell
            
        case is Room:
            let cell = tableView.dequeueReusableCellWithIdentifier("RoomTableViewCell",
                forIndexPath: indexPath) as! RoomTableViewCell
            
            if let room = item as? Room {
                cell.setRoom(room)
            }
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("PotTableViewCell",
                forIndexPath: indexPath) as! PotTableViewCell
            return cell
        }
    }
}
