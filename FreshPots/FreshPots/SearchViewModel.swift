//
//  SearchViewModel.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/30/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

/// TableView sections
private enum Sections: Int {
    case Pots, Teams, Rooms, Users
}

/// SearchTableView scopes
private enum Scopes: Int {
    case Pots, Teams, Rooms, Users, All
}

protocol SearchViewModelDelegate {
    func viewModelUpdated()
}

/// ViewModel that contains Pot, Team, Room and User arrays
/// and handles the filtering and searching of arrays.
///
class SearchViewModel: NSObject, DataSourceDelegate {
    var delegate :SearchViewModelDelegate?
    
    private var dataSource = SearchViewModelDataSource()
    
    private var filteredPots = [Pot]()
    private var filteredTeams = [Team]()
    private var filteredRooms = [Room]()
    private var filteredUsers = [User]()
    
    override init() {
        super.init()
        dataSource.delegate = self
    }
    
    func reload() {
        self.dataSource.updateDataSource()
    }
}

// MARK: - DataSourceDelegate -
extension SearchViewModel {
    func dataSourceUpdated() {
        self.delegate?.viewModelUpdated()
    }
}

// MARK: - Filters -
extension SearchViewModel {
    
    /// Find items that match the current search text and scope.
    ///
    func filterContentForSearchText(searchText: String, scope: Int) {
        resetFiltered()
        
        switch scope {
        case Scopes.All.rawValue:
            filter(searchText)
        case Scopes.Pots.rawValue:
            filterPots(searchText)
            break
        case Scopes.Teams.rawValue:
            filterTeams(searchText)
            break
        case Scopes.Rooms.rawValue:
            filterRooms(searchText)
            break
        case Scopes.Users.rawValue:
            filterUsers(searchText)
            break
        default: break
        }
    }
    
    private func resetFiltered() {
        filteredPots.removeAll()
        filteredTeams.removeAll()
        filteredRooms.removeAll()
        filteredUsers.removeAll()
    }
    
    private func filter(searchText: String) {
        filterPots(searchText)
        filterTeams(searchText)
        filterRooms(searchText)
        filterUsers(searchText)
    }
    
    private func filterPots(searchText: String) {
        filteredPots = dataSource.pots.filter({ (pot: Pot) -> Bool in
            let stringMatch = pot.name.lowercaseString.containsString(searchText.lowercaseString)
            return stringMatch
        })
    }
    
    private func filterTeams(searchText: String) {
        filteredTeams = dataSource.teams.filter({ (team: Team) -> Bool in
            let stringMatch = team.name.lowercaseString.containsString(searchText.lowercaseString)
            return stringMatch
        })
        
    }
    
    private func filterUsers(searchText: String) {
        filteredUsers = dataSource.users.filter({ (user: User) -> Bool in
            let stringMatch = user.firstName.lowercaseString.containsString(searchText.lowercaseString)
            return stringMatch
        })
    }
    
    private func filterRooms(searchText: String) {
        filteredRooms = dataSource.rooms.filter({ (room: Room) -> Bool in
            let stringMatch = room.name.lowercaseString.containsString(searchText.lowercaseString)
            return stringMatch
        })
    }
}

// MARK: - TableView -
extension SearchViewModel {
    func numberOfSectionsInTableView() -> Int {
        var sectionCount = 0
        let itemArray :[Int] = [
            dataSource.pots.count, dataSource.teams.count, dataSource.rooms.count, dataSource.users.count
        ]
        
        for item in itemArray {
            if item > 0 {
                sectionCount++
            }
        }
        
        return sectionCount
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        switch section {
        case Sections.Pots.rawValue:
            return dataSource.pots.count
        case Sections.Teams.rawValue:
            return dataSource.teams.count
        case Sections.Users.rawValue:
            return dataSource.users.count
        case Sections.Rooms.rawValue:
            return dataSource.rooms.count
        default: return 0
        }
    }
    
    func titleForItemAtIndexPath(indexPath: NSIndexPath) -> String? {
        switch indexPath.section {
        case Sections.Pots.rawValue:
            return dataSource.pots[indexPath.row].name
        case Sections.Teams.rawValue:
            return dataSource.teams[indexPath.row].name
        case Sections.Users.rawValue:
            return dataSource.users[indexPath.row].firstName + " " + dataSource.users[indexPath.row].lastName
        case Sections.Rooms.rawValue:
            return dataSource.rooms[indexPath.row].name
        default: return nil
        }
    }
    
    func titleForSection(section: Int) -> String? {
        switch section {
        case Sections.Pots.rawValue:
            return "Pots"
        case Sections.Teams.rawValue:
            return "Teams"
        case Sections.Users.rawValue:
            return "Users"
        case Sections.Rooms.rawValue:
            return "Rooms"
        default: return nil
        }
    }
    
    func itemForRowAtIndexPath(indexPath: NSIndexPath) -> DataModel? {
        switch indexPath.section {
        case Sections.Pots.rawValue:
            return dataSource.pots[indexPath.row]
        case Sections.Teams.rawValue:
            return dataSource.teams[indexPath.row]
        case Sections.Users.rawValue:
            return dataSource.users[indexPath.row]
        case Sections.Rooms.rawValue:
            return dataSource.rooms[indexPath.row]
        default: return nil
        }
    }
}

// MARK: - SearchTableView -
extension SearchViewModel {
    func numberOfSectionsInSearchTableView() -> Int {
        var sectionCount = 0
        let itemArray :[Int] = [
            filteredPots.count, filteredTeams.count, filteredRooms.count, filteredUsers.count
        ]
        
        for item in itemArray {
            if item > 0 {
                sectionCount++
            }
        }
        
        return sectionCount
    }
    
    func numberOfRowsInFilteredSection(section: Int) -> Int {
        switch section {
        case Sections.Pots.rawValue:
            return filteredPots.count
        case Sections.Teams.rawValue:
            return filteredTeams.count
        case Sections.Users.rawValue:
            return filteredUsers.count
        case Sections.Rooms.rawValue:
            return filteredRooms.count
        default: return 0
        }
    }
    
    func titleForFilteredItemAtIndexPath(indexPath: NSIndexPath) -> String? {
        switch indexPath.section {
        case Sections.Pots.rawValue:
            return filteredPots[indexPath.row].name
        case Sections.Teams.rawValue:
            return filteredTeams[indexPath.row].name
        case Sections.Users.rawValue:
            return filteredUsers[indexPath.row].firstName + " " + filteredUsers[indexPath.row].lastName
        case Sections.Rooms.rawValue:
            return filteredRooms[indexPath.row].name
        default: return nil
        }
    }
    
    func itemForFilteredRowAtIndexPath(indexPath: NSIndexPath) -> DataModel? {
        switch indexPath.section {
        case Sections.Pots.rawValue:
            return filteredPots[indexPath.row]
        case Sections.Teams.rawValue:
            return filteredTeams[indexPath.row]
        case Sections.Users.rawValue:
            return filteredUsers[indexPath.row]
        case Sections.Rooms.rawValue:
            return filteredRooms[indexPath.row]
        default: return nil
        }
    }
    
    func titleForFilteredSection(section: Int) -> String? {
        switch section {
        case Sections.Pots.rawValue:
            if filteredPots.count > 0 {
                return "Pots"
            }
            return nil
        case Sections.Teams.rawValue:
            if filteredTeams.count > 0 {
                return "Teams"
            }
            return nil
        case Sections.Users.rawValue:
            if filteredUsers.count > 0 {
                return "Users"
            }
            return nil
        case Sections.Rooms.rawValue:
            if filteredRooms.count > 0 {
                return "Rooms"
            }
            return nil
        default: return nil
        }
    }
    
    func scopeButtonTitles() -> [String] {
        return ["Pots", "Teams", "Rooms", "Users", "All"]
    }
}

