//
//  SearchViewModelDataSource.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/11/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

protocol DataSourceDelegate {
    func dataSourceUpdated()
}

class SearchViewModelDataSource: NSObject {
    var delegate :DataSourceDelegate?
    
    var users :[User]
    var pots  :[Pot]
    var teams :[Team]
    var rooms :[Room]
    
    override init() {
        users = [User]()
        pots = [Pot]()
        teams = [Team]()
        rooms = [Room]()
        
        super.init()
        
        updateDataSource()
    }
    
    func updateDataSource() {
        DataModelService<User>.getModels { users in
            self.users = users
            self.delegate?.dataSourceUpdated()
        }
        
        DataModelService<Pot>.getModels { pots in
            self.pots = pots
            self.delegate?.dataSourceUpdated()
        }
        
        DataModelService<Team>.getModels { teams in
            self.teams = teams
            self.delegate?.dataSourceUpdated()
        }
        
        DataModelService<Room>.getModels { rooms in
            self.rooms = rooms
            self.delegate?.dataSourceUpdated()
        }
    }

}
