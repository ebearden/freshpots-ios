//
//  SettingsViewController.swift
//  FreshPots
//
//  Created by Elvin Bearden on 11/14/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    let viewModel: SettingsViewModel
    
    required init?(coder aDecoder: NSCoder) {
        self.viewModel = SettingsViewModel()
        
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SettingsCell", forIndexPath:indexPath) as UITableViewCell
        
        cell.textLabel?.text = self.viewModel.descriptionForCell(indexPath)
        
        return cell
    }
}
