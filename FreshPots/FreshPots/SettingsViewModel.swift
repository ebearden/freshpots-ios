//
//  SettingsViewModel.swift
//  FreshPots
//
//  Created by Elvin Bearden on 11/14/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

struct SettingsViewModel {
    enum Settings: Int {
        case Search
        case CreateTeam
        case CreatePot
        case Profile
        case Notification
    }
    
    func descriptionForCell(indexPath: NSIndexPath) -> String {
        switch indexPath.row {
            case Settings.Search.rawValue:
                return "Search"
                
            case Settings.CreateTeam.rawValue:
                return "Create/Join Team"
            
            case Settings.CreatePot.rawValue:
                return "Create Pot"
            
            case Settings.Profile.rawValue:
                return "Profile"
            
            case Settings.Notification.rawValue:
                return "Notification"
            
            default:
                return ""
        }
    }
}
