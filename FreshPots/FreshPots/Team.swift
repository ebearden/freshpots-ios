//
//  Team.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/26/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Team : DataModel, CustomStringConvertible {
    /// Identifier generated from server
    var identifier :String
    
    /// The name of the team
    var name :String
    
    /// The teams profile image
    var profileImage :String
    
    /// The name of the room
    var location :String
}

// MARK: - DataModel Protocol -
extension Team {
    static func make(json: JSON) -> DataModel {
        return Team(
            identifier: json["id"].stringValue,
            name: json["name"].stringValue,
            profileImage: json["profile_image"].stringValue,
            location: json["location"].stringValue
        )
    }
    
    func jsonDictionary() -> Dictionary<String, AnyObject> {
        return [
            "id": identifier,
            "name": name,
            "profile_image": profileImage,
            "location": location
        ]
    }
}

// MARK: - CustomStringConvertible -
extension Team {
    var description :String {
        return "id: \(identifier)\name: \(name)\nlocation: \(location)\nprofileImage:\(profileImage)"
    }
}