//
//  TeamTableViewCell.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/19/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var teamLogoImageView: UIImageView!

    private var team :Team?
    
    func setTeam(team: Team) {
        nameLabel?.text = team.name
        setLogoImage(team.profileImage)
    }
}

// MARK: - Private methods -
extension TeamTableViewCell {
    private func setLogoImage(imageUrlString: String) {
        guard imageUrlString != "" else {
            teamLogoImageView.image = nil
            return
        }
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            let data = NSData(contentsOfURL: NSURL(string:imageUrlString)!)
            
            dispatch_async(dispatch_get_main_queue()) {
                self.teamLogoImageView.image = UIImage(data: data!)
                self.maskImageView(self.teamLogoImageView)
            }
        }
    }
    
    private func maskImageView(imageView: UIImageView) {
        let path = UIBezierPath(ovalInRect: imageView.bounds)
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        imageView.layer.mask = mask
    }
}