//
//  User.swift
//  FreshPots
//
//  Created by Elvin Bearden on 9/26/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit
import SwiftyJSON

struct User : DataModel, CustomStringConvertible {
    /// Identifier generated from server
    var identifier :String
    
    /// The email address of the user
    var emailAddress :String
    
    /// The first name of the user
    var firstName :String
    
    /// The last name of the user
    var lastName :String
    
    /// The profile image url of the user
    var profileImage :String
    
    /// An array of pots the user subscribes to
    var pots :[Pot]
}

// MARK: - DataModel Protocol -
extension User {
    static func make(json: JSON) -> DataModel {
        var array = [Pot]()
        if let potArray = json["pots"].array {
            for item in potArray {
                let factory = DataModelFactory.factoryFor(DataModelType.Pot)
                let pot = factory(item) as! Pot
                array.append(pot)
            }
        }
        
        return User(
            identifier: json["id"].stringValue,
            emailAddress: json["email_address"].stringValue,
            firstName: json["first_name"].stringValue,
            lastName: json["last_name"].stringValue,
            profileImage: json["profile_image"].stringValue,
            pots: array
        )
    }
    
    func jsonDictionary() -> Dictionary<String, AnyObject> {
        return [
            "id": identifier,
            "email_address": emailAddress,
            "first_name": firstName,
            "last_name": lastName,
            "profile_image": profileImage
        ]
    }
}


// MARK: - CustomStringConvertible -
extension User {
    var description :String {
        return "id: \(identifier)\nemailAddress: \(emailAddress)\n" +
        "firstName: \(firstName)\nlastName:\(lastName)\nprofileImage:\(profileImage)"
    }
}
