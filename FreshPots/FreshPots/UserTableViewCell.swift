//
//  UserTableViewCell.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/13/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var profileImageView: UIImageView!
    
    private var user :User?
    
    func setUser(user: User) {
        nameLabel?.text = user.firstName + " " + user.lastName
        setUserImage(user.profileImage)
    }
}

// MARK: - Private methods -
extension UserTableViewCell {
    private func setUserImage(imageUrlString: String) {
        guard imageUrlString != "" else {
            profileImageView.image = nil
            return
        }
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            let data = NSData(contentsOfURL: NSURL(string:imageUrlString)!)
        
            dispatch_async(dispatch_get_main_queue()) {
                self.profileImageView.image = UIImage(data: data!)
                self.maskImageView(self.profileImageView)
            }
        }
    }
    
    private func maskImageView(imageView: UIImageView) {
        let path = UIBezierPath(ovalInRect: imageView.bounds)
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        imageView.layer.mask = mask
    }
}