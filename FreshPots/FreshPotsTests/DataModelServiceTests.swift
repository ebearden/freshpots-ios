//
//  DataModelServiceTests.swift
//  FreshPots
//
//  Created by Elvin Bearden on 10/11/15.
//  Copyright © 2015 FreshPots. All rights reserved.
//

import XCTest

class DataModelServiceTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetUser() {
        let expectation = expectationWithDescription("testGetUser")
        DataModelService<User>.getModelWithId("1") { (user) -> Void in
            XCTAssertFalse(user.identifier.isEmpty)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetUsers() {
        let expectation = expectationWithDescription("testGetUsers")
        DataModelService<User>.getModels { (users) -> Void in
            XCTAssert(users.count > 0)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetTeam() {
        let expectation = expectationWithDescription("testGetTeam")
        DataModelService<Team>.getModelWithId("1") { (team) -> Void in
            XCTAssertFalse(team.identifier.isEmpty)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetTeams() {
        let expectation = expectationWithDescription("testGetTeams")
        DataModelService<Team>.getModels { (teams) -> Void in
            XCTAssert(teams.count > 0)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetRoom() {
        let expectation = expectationWithDescription("testGetRoom")
        DataModelService<Room>.getModelWithId("1") { (room) -> Void in
            XCTAssertFalse(room.identifier.isEmpty)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetRooms() {
        let expectation = expectationWithDescription("testGetRooms")
        DataModelService<Room>.getModels { (rooms) -> Void in
            XCTAssert(rooms.count > 0)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetPot() {
        let expectation = expectationWithDescription("testGetPot")
        DataModelService<Pot>.getModelWithId("4") { (pot) -> Void in
            XCTAssertFalse(pot.identifier.isEmpty)
            expectation.fulfill()
        }
        
        wait()
    }
    
    func testGetPots() {
        let expectation = expectationWithDescription("testGetPots")
        DataModelService<Pot>.getModels { (pots) -> Void in
            XCTAssert(pots.count > 0)
            expectation.fulfill()
        }
        
        wait()
    }
}

extension DataModelServiceTests {
    private func wait() {
        waitForExpectationsWithTimeout(5) { (error) -> Void in
            if let _ = error {
                XCTFail("Timed out")
            }
        }
    }
}
